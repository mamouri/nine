const app = require('../app.js');
const request = require('supertest');

test('API handles filtering properly', async () => {
   const payload = {
      "payload": [
         {
            "drm": true,
            "image": {
               "showImage": "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg"
            },
            "language": "English",
            "primaryColour": "#ff7800",
            "slug": "show/16kidsandcounting",
            "title": "16 Kids and Counting",
            "tvChannel": "GEM"
         },
         {
            "slug": "show/seapatrol",
            "title": "Sea Patrol",
            "tvChannel": "Channel 9"
         },
         {
            "drm": true,
            "episodeCount": 2,
            "genre": "Reality",
            "image": {
               "showImage": "http://mybeautifulcatchupservice.com/img/shows/TheTaste1280.jpg"
            },
            "language": "English",
            "primaryColour": "#df0000",
            "seasons": [
               {
                  "slug": "show/thetaste/season/1"
               }
            ],
            "slug": "show/thetaste",
            "title": "The Taste",
            "tvChannel": "GEM"
         }
      ]
   };

   const expected = [
      {
         image: {showImage: "http://mybeautifulcatchupservice.com/img/shows/TheTaste1280.jpg"},
         slug: 'show/thetaste',
         title: 'The Taste'
      }
   ];

   const response = await request(app.callback()).post('/v1').send(payload).expect(200);

   expect(response.body.response).toEqual(expected);
});

test('API handles malformed JSON gracefully', async () => {
   const payload = 'garbage JSON';

   const expected = {
      "error": "Could not decode request: JSON parsing failed"
   };

   const response = await request(app.callback())
      .post('/v1')
      .type('json')
      .send(payload)
      .expect(400);

   expect(response.body).toEqual(expected);
});
