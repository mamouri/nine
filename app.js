const Koa = require('koa');
const Json = require('koa-json');
const Router = require('koa-router');
const BodyParser = require('koa-bodyparser');
const R = require('ramda');

app = new Koa();

// Filter shows that have DRM and are released (have at least one episode)
const filterDRM = R.filter(
   R.both(
      R.propEq('drm', true),
      R.propSatisfies(x => x > 0, 'episodeCount')
   )
);

// Extract fields required for output
const transform = R.map(
   R.pick(['image', 'slug', 'title'])
);

// Router
const router = new Router();
router.post('/v1', async (ctx, next) => {
   const body = ctx.request.body;
   const response = R.pipe(filterDRM, transform)(body.payload);

   ctx.body = {response}
});

// Middlewares
app
   .use((ctx, next) => {
      return next().catch(err => {
         ctx.status = err.status;
         ctx.body = {
            error: err.message
         };
      });
   })
   .use(Json({}))
   .use(BodyParser({
      onerror: (err, ctx) => {
         ctx.throw(400, "Could not decode request: JSON parsing failed")
      }
   }))
   .use(router.routes())
   .use(router.allowedMethods());

module.exports = app;
