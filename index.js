const app = require('./app');

const server = app.listen(3000).on("error", err => {
   console.error(err);
});

module.exports = server;
