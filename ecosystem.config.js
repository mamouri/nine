module.exports = {
  apps : [{
    name: 'Nine API',
    script: 'index.js',

    args: '',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    production : {
      user : 'node',
      host : '212.83.163.1',
      ref  : 'origin/master',
      repo : 'git@bitbucket.org:mamouri/nine.git',
      path : '/home/ubuntu/nine',
      'post-deploy' : 'npm test && npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
